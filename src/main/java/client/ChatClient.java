package client;

import java.io.*; // Ce package fournit des classes pour la gestion des entrées/sorties.
import java.net.Socket;//Ce package fournit des classes pour la communication réseau.

import com.fasterxml.jackson.databind.ObjectMapper;//Ce package fournit des classes pour la sérialisation/désérialisation de données JSON.

import message.Message;//Ce package contient la classe Message, qui est une classe personnalisée pour stocker des informations de message.

import message.Message;

import static message.Message.*;//Cette instruction statique est utilisée pour importer toutes les méthodes statiques de la classe Message. Cela permet d'utiliser ces méthodes sans avoir à qualifier chaque appel avec le nom de la classe.

/**
 * The class ChatClient is the main program for the client of our chat application
 * The server's port is defined as a constant
 */
public class ChatClient {
    private DataInputStream in;
    private DataOutputStream out;
    private Socket sock;
    private String username;
    private boolean serverStop = false;
    private final String HOST = "localhost";
    private final int PORT = 12138;

    public static void main(String[] args) {
        ChatClient client = new ChatClient();
        client.go();
    }

    /**
     * The client connects to the server then starts 2 threads to send and receive messages
     */
    public void go() {
        setUp();
        Thread readerThread = new IncomingReader();
        readerThread.start();
        Thread writerThread = new OutgoingWriter();
        writerThread.start();
    }

    /**
     * The function setUp establishes the connection between the client and the server
     * then inscript with a valid username
     */
    private void setUp() {
        try {
            sock = new Socket(HOST, PORT);
            in = new DataInputStream(sock.getInputStream());
            out = new DataOutputStream(sock.getOutputStream());
            System.out.println("Networking established");

            System.out.println("Inscription: enter a valid username");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            Message clientMessage = new Message("","", INSCRIPTION);
            Message serverMessage;
            do{
                username = br.readLine();
                clientMessage.setSender(username);
                out.writeUTF(clientMessage.toJson());
                String JsonMessage= in.readUTF();
                serverMessage = toMessage(JsonMessage);
            }while(serverMessage.getCode() != ACCEPTED);
            System.out.println("Inscription succeed");
            System.out.println("Users " + serverMessage.getContent() + " are in the chat room");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * The IncomingReader is a thread running an infinite loop to receive messages from the server and print them out, until the server close the connection
     */
    public class IncomingReader extends Thread {
        public void run() {
            String JsonMessage ;
            try {
                boolean clientStop = false;
                while (!clientStop && !serverStop) {
                    JsonMessage = in.readUTF();
                    Message m = toMessage(JsonMessage);
                    switch (m.getCode()) {
                        case SUCCESS:
                            System.out.println("\t[" + m.getSender() + "]:" + m.getContent());
                            break;
                        case ADDED:
                            System.out.println("\n\t[" + m.getSender() + "] joined the server\n");
                            break;
                        case DISCONNECT:
                            System.out.println("\n\t[" + m.getSender() + "] quit the server\n");
                            break;
                        case STOP:
                            System.out.println("\tConversation finished\n");
                            clientStop = true;
                            break;
                        case ERROR:
                            System.out.println("\n\tAn error occurred\n");
                            break;
                        default:
                            System.out.println("\n\tUnknown type of server message\n");
                            break;
                    }
                }
            } catch (IOException ex) {
                // ex.printStackTrace();
                System.out.println("Server stopped");
                serverStop = true;
            }
        }
    }

    /**
     * The OutgoingWriter is a thread running an infinit loop to send messages to the server, until the user types "exit" to close the connection
     */
    public class OutgoingWriter extends Thread {
        public void run() {
            String content;
            BufferedReader brCli = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Enter text to be sent to Server\n");
            try {
                while (!serverStop) {
                    content = brCli.readLine();
                    Message clientMessage;
                    if(content.equals("exit")) {
                        clientMessage = new Message(username, "", QUIT);
                        out.writeUTF(clientMessage.toJson());
                        break;
                    }
                    clientMessage = new Message(username, content, MESSAGE);
                    out.writeUTF(clientMessage.toJson());
                }
            } catch (IOException ex) {
                // ex.printStackTrace();
                System.out.println("Server stopped");
            }
        }
    }
}
