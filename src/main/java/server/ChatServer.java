package server;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * The class ChatServer is the main program for the server of our chat application
 * A specific port is defined as a constant to start the server
 */
public class ChatServer {
    private final int PORT = 12138;

    public static void main(String[] args) {
        new ChatServer().go();
    }

    /**
     * The ServerSocket is started at the defined port
     * then enter an infinite loop to accept connected clients
     * a ServerMessageReceptor will be started on a new thread for each client
     */
    public void go() {
        try {
            ServerSocket serverSock = new ServerSocket(PORT);
            System.out.println("Server started at port: "+PORT);
            while (true) {
                Socket clientSocket = serverSock.accept();
                Thread t = new ServerMessageReceptor(clientSocket);
                t.start();
                System.out.println("New connection from "+clientSocket);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
