package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import message.Message;

import static message.Message.*;

/**
 * The class ServerMessageReceptor is implemented as a thread to treat the inscription and the messaging of client
 */
public class ServerMessageReceptor extends Thread{
    private String username;
    private Socket sock;
    private DataInputStream in;
    private DataOutputStream out;

    /**
     * The creater takes the client's socket as argument
     * and initialize the InputStream and the OutputStream between the client and the server
     * @param sock
     */
    public ServerMessageReceptor(Socket sock){
        this.sock = sock;
        this.username = "";
        try{
            in = new DataInputStream(sock.getInputStream());
            out = new DataOutputStream(sock.getOutputStream());
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    public String getUsername(){return username;}
    public Socket getSock(){return sock;}
    public DataInputStream getIn(){return in;}
    public DataOutputStream getOut(){return out;}

    /**
     * A conversation starts with the inscription of client
     * then the server transfers the messages of the client to all the other clients
     */
    public void run(){
        try{
            Message serverMessage = new Message();
            String JsonMessage = in.readUTF();
            Message clientMessage = Message.toMessage(JsonMessage);
            username = clientMessage.getSender();

            /**
             * During the inscription, the server will asked for a valid username from the client
             */
            while(!ServerAdministrator.addClient(username, this)){
                System.out.println("Username ["+username+"] already exists, refusing");
                serverMessage.setCode(Message.REFUSED);
                out.writeUTF(serverMessage.toJson());
                JsonMessage = in.readUTF();
                clientMessage = Message.toMessage(JsonMessage);
                username = clientMessage.getSender();
            }
            
            serverMessage.setCode(ACCEPTED);
            serverMessage.setContent(ServerAdministrator.listUsernames());
            out.writeUTF(serverMessage.toJson());
            ServerAdministrator.showAllClients();
            System.out.println("User [" + username + "] added\n");
            serverMessage.setSender(username);
            serverMessage.setCode(Message.ADDED);
            ServerAdministrator.tellEveryone(serverMessage);

            boolean stop = false;
            /**
             * The server starts an infinite loop until the client exit the server
             */
            while(!stop){
                JsonMessage= in.readUTF();
                serverMessage = Message.toMessage(JsonMessage);
                switch (serverMessage.getCode()){
                    case QUIT :
                        serverMessage.setCode(DISCONNECT);
                        ServerAdministrator.tellEveryone(serverMessage);
                        serverMessage.setCode(STOP);
                        out.writeUTF(serverMessage.toJson());
                        stop = true;
                        break;
                    case MESSAGE:
                        serverMessage.setCode(SUCCESS);
                        ServerAdministrator.tellEveryone(serverMessage);
                        break;
                    default:
                        serverMessage.setCode(ERROR);
                        out.writeUTF(serverMessage.toJson());
                        break;
                }
            }
            if(ServerAdministrator.removeClient(username))
                System.out.println("User [" + username + "] removed\n");
            in.close();
            out.close();
        } catch (IOException e) {
            if(ServerAdministrator.removeClient(username))
                System.out.println("User [" + username + "] quitted unexpectedly, removed\n");
            else System.out.println("User [" + username + "] quitted unexpectedly, but not found in the list\n");
            Message serverMessage = new Message(username,"",DISCONNECT);
            ServerAdministrator.tellEveryone(serverMessage);
            // throw new RuntimeException(e);
        }
    }
}
