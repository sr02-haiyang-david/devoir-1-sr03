package server;

import message.Message;

import java.util.Map;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

/**
 * The class ServerAdministrator stores the username and the socket of all connecting clients in a HashMap
 */
public class ServerAdministrator{
    public static HashMap<String, ServerMessageReceptor> clients = new HashMap<String, ServerMessageReceptor>();

    /**
     * The function showAllClients prints the list of clients for the server
     */
    public static void showAllClients(){
        System.out.println("\nList of users:");
        Iterator<Map.Entry<String,ServerMessageReceptor>> it = clients.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<String, ServerMessageReceptor> c = it.next();
            System.out.println("\tUsername: ["+c.getKey()+"], Socket: "+c.getValue().getSock());
        }
    }

    /**
     * The function listUsernames prepares a user list for a new added client
     * @return a String listing all the username connecting the server
     */
    public static String listUsernames(){
        Iterator<Map.Entry<String,ServerMessageReceptor>> it = clients.entrySet().iterator();
        String list = "";
        while(it.hasNext()){
            Map.Entry<String, ServerMessageReceptor> user = it.next();
            list += "[" + user.getKey() + "],";
        }
        return list;
    }

    /**
     * The function isValid checks the validity of a username by looking up the list of existing usernames
     * @param username
     * @return validity of username
     */
    public static boolean isValid(String username){
        return (!clients.containsKey(username));
    }

    /**
     * The function addClient tries to add the username and the socket of a client into the HashMap list.
     * It checkes the validity of username and return if it can add this username or not
     * @param username
     * @param socket
     * @return success or not adding this client
     */
    public static boolean addClient(String username, ServerMessageReceptor socket){
        if(isValid(username)){
            clients.put(username, socket);
            return true;
        }
        else return false;
    }

    /**
     * The function removeClient tries to remove the username from the list.
     * @param username
     * @return success or not removing this client
     */
    public static boolean removeClient(String username){
        ServerMessageReceptor res = clients.remove(username);
        if(res == null) return false;
        else {
            res.interrupt();
            return true;
        }
    }

    /**
     * The function tellEveryone send a message to all the client's socket
     * @param message
     */
    public static void tellEveryone(Message message){
        Iterator<Map.Entry<String,ServerMessageReceptor>> it = clients.entrySet().iterator();
        DataOutputStream out;
        while(it.hasNext()){
            Map.Entry<String, ServerMessageReceptor> client = it.next();
            out = client.getValue().getOut();
            
            try{
                out.writeUTF(message.toJson());
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}
