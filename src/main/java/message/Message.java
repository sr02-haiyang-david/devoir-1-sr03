package message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The class Message implements the message sent between the clients and the server
 */
public class Message{
    private String sender;
    private String content;
    private int code;

    public static final int INSCRIPTION = 101;
    public static final int MESSAGE = 102;
    public static final int QUIT = 103;
    public static final int SUCCESS = 200;
    public static final int ADDED = 201;
    public static final int DISCONNECT = 202;
    public static final int ACCEPTED = 400;
    public static final int REFUSED = 401;
    public static final int ERROR = 402;
    public static final int STOP = 403;

    public Message(){
        this.sender = "";
        this.content = "";
        this.code = 0;
    }
    public Message(String _sender, String _content, int _code){
        this.sender = _sender;
        this.content = _content;
        this.code = _code;
    }

    public int getCode(){return code;}
    public String getSender(){return sender;}
    public String getContent(){return content;}
    public void setCode(int c){this.code = c;}
    public void setSender(String s){this.sender = s;}
    public void setContent(String c){this.content = c;}

    /**
     * The method toJson transform the Message object into a Json form string and return it
     * @return the Message in Json form string
     */
    public String toJson(){
        ObjectMapper mapper = new ObjectMapper();
        try{
            return mapper.writeValueAsString(this);
        }catch(JsonProcessingException e){
            e.printStackTrace();
            return "JsonError";
        }
    }

    /**
     * The method toMessage restore the Message object from a Json form string of a message
     * @param jsonMessage
     * @return the Message object restored
     */
    public static Message toMessage(String jsonMessage){
        ObjectMapper mapper = new ObjectMapper();
        Message message = new Message();
        try{
            message = mapper.readValue(jsonMessage, Message.class);
        }catch(JsonProcessingException exception){
            exception.printStackTrace();
        }
        return message;
    }
}
