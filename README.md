# Rapport de Devoir n°1- Socket - SR03

## Prise en main du développement d’une Application de chat multi-thread en Java

Instructeur : Grégory Quentin

Auteur : Haiyang MA, David SANDY ROJAS

Date : Avril 2023

## Introduction

Les sockets et le multi-threading sont des éléments clés pour le développement d'applications de chat groupal en Java. Les sockets permettent la communication entre le serveur et les clients, tandis que le multi-threading permet de gérer plusieurs connexions de clients simultanément.

Lorsqu'un client se connecte au serveur de chat, une nouvelle connexion est établie à travers un socket. Le serveur crée un thread pour gérer cette connexion client, ce qui permet au serveur de traiter plusieurs connexions de clients simultanément. Chaque connexion client est gérée dans son propre thread, ce qui permet de traiter les messages de chaque client de manière indépendante.

Les sockets permettent également aux clients de communiquer entre eux en envoyant des messages au serveur de chat qui relaie les messages à tous les autres clients connectés. Les clients peuvent également recevoir les messages envoyés par d'autres clients en écoutant les connexions entrantes du serveur de chat.

En résumé, les sockets et le multi-threading sont des éléments clés pour le développement d'applications de chat groupal en Java. Ils permettent de gérer plusieurs connexions de clients simultanément et une communication bidirectionnelle entre les clients et le serveur de chat.

## Contexte du projet

### Explication du projet

L'objectif de ce projet est de développer une application de chat Client/Serveur via les sockets. Le chat permettra à un groupe de participants de discuter en temps réel à travers une interface console. La communication entre les clients et le serveur sera établie via des sockets, ce qui permettra d'établir une communication bidirectionnelle entre eux. Les participants pourront se connecter, envoyer et recevoir des messages, consulter la liste des utilisateurs connectés et se déconnecter de l'application.

### Concepts

Le développement d'une application de chat Client/Serveur via les sockets nécessite une compréhension approfondie des concepts de programmation réseau et de manipulation de sockets. La programmation réseau implique la communication entre plusieurs ordinateurs via un réseau, tandis que la manipulation de sockets implique l'envoi et la réception de messages via des canaux de communication établis entre les clients et le serveur.

### Objectifs fixés

Le principal objectif de ce projet est d'acquérir une expérience pratique de la programmation réseau et de la manipulation de sockets en développant une application de chat Client/Serveur. Ce projet permettra également de comprendre le fonctionnement des communications entre les clients et le serveur dans un environnement distribué. Les développeurs apprendront également à gérer les erreurs et les exceptions qui peuvent survenir pendant l'exécution de l'application.

### Cas d'utilisation théorique

L'application de chat Client/Serveur permettra de gérer plusieurs cas d'utilisation théoriques. Par exemple, lorsqu'un client se connecte au serveur, il envoie une demande de connexion au serveur via un socket. Le serveur accepte cette demande et établit une connexion avec le client. Une fois connecté, le client peut envoyer des messages à tous les autres utilisateurs connectés au serveur. Le serveur diffusera ensuite le message à tous les autres clients connectés. Pour faciliter la compréhension de ces cas d'utilisation, des schémas sont au dessous pour chaque étape de l'application. Ces schémas aideront également les développeurs à diagnostiquer et à résoudre les problèmes qui pourraient survenir.

### Schéma

```mermaid
sequenceDiagram
actor a as Alice
participant s as Server
actor b as Bob

note over s: start

a->>s: connect
s->>a: accept
s--)a: notify

b->>s: connect
s->>b: accept
 par
 s--)a: notify
 s--)b: notify
 end

loop Messaging
a-)s: send
 par
 s--)a: distribut
 s--)b: distribut
 end
end

b->>s: quit
s->>b: disconnect
 par
 s--)a: notify
 s--)b: notify
 end
```

## Conception des classes

### Classe ChatClient.java

La classe ChatClient utilise les packages Java IO et Java.net pour la communication réseau. Il utilise également le package com.fasterxml.jackson.databind pour la sérialisation et la désérialisation de données JSON.

Le code définit une classe ChatClient, qui contient plusieurs méthodes pour établir la connexion entre le client et le serveur, inscrire l'utilisateur avec un nom d'utilisateur valide et gérer l'envoi et la réception de messages.

La méthode go() établit la connexion avec le serveur et démarre deux threads pour envoyer et recevoir des messages. La méthode setUp() établit la connexion entre le client et le serveur, inscrit l'utilisateur avec un nom d'utilisateur valide et affiche la liste des utilisateurs connectés.

La classe IncomingReader est un thread qui s'exécute en boucle pour recevoir les messages du serveur et les afficher. La classe OutgoingWriter est un autre thread qui s'exécute en boucle pour envoyer les messages au serveur. Les deux threads s'arrêtent lorsque le serveur ferme la connexion ou que l'utilisateur tape "exit" pour quitter le chat.

Les exceptions de flux sont gérées par le bloc try-catch. S'il y a une déconnexion de serveur sans que le client soit prévenu, les threads de client vont arrêter et imprimer un message indiquant la déconncexion inattendue du serveur.

### Classe ChatServer.java

La classe ChatServer permet de démarrer un serveur sur un port spécifique et ensuite entre dans une boucle infinie pour accepter les clients connectés.

Le serveur accepte les connexions des clients en créant un objet Socket pour chaque client. Ensuite, il démarre un nouveau thread pour chaque client en créant un objet ServerMessageReceptor qui sera chargé de recevoir et d'envoyer des messages du client.

La classe ChatServer possède une constante PORT qui définit le port du serveur. Dans la méthode go(), on crée un objet ServerSocket sur le port défini et on démarre une boucle infinie pour accepter les clients. À chaque itération de la boucle, le serveur accepte une connexion d'un client en créant un objet Socket pour le client.

Ensuite, on crée un nouveau thread en créant un objet ServerMessageReceptor pour le client, et on démarre le thread en appelant la méthode start().

Finalement, on imprime un message dans la console pour informer que l'on a établi une nouvelle connexion avec un client.

### Classe Message.java

La classe message s'occupe d’implémenter le message envoyé entre les clients et le serveur dans notre application de chat.

Cette classe possède plusieurs attributs : le nom de l'expéditeur, le contenu du message et un code de statut. Le code de statut permet d'identifier le type de message envoyé, par exemple un message d'inscription, un message de conversation, ou un message de déconnexion.

La classe comporte également plusieurs constantes qui représentent différents codes de statut. Ces constantes permettent d'identifier le type de message reçu ou envoyé.
La classe a deux méthodes : toJson et toMessage. La méthode toJson transforme l'objet Message en une chaîne de caractères au format Json et la renvoie. La méthode toMessage restaure l'objet Message à partir d'une chaîne de caractères au format Json d'un message. Ces deux méthodes utilisent la bibliothèque Jackson pour la conversion entre les objets de type Java et Json.

En d'autres mots, la classe Message est une classe de base qui représente les messages envoyés entre les clients et le serveur. Elle permet de stocker les informations essentielles des messages et de les convertir en format Json pour la communication entre les différents composants de l'application de chat.

### Classe ServerMessageReceptor.java

La classe ServerMessageReceptor dans le package “server” est implémentée comme un thread pour gérer l'inscription et les échanges de messages avec les clients.

Le constructeur prend comme argument le socket du client et initialise le flux d'entrée et de sortie entre le client et le serveur.

Une conversation commence par l'inscription du client. Le serveur transfère ensuite les messages du client à tous les autres clients.

Pendant l'inscription, le serveur demandera un nom d'utilisateur valide au client. Si le nom d'utilisateur est déjà pris, le serveur en informe le client et demande un nouveau nom d'utilisateur.

Une fois l'inscription terminée, le serveur envoie un message d'acceptation au client et informe tous les autres clients qu'un nouvel utilisateur a rejoint le chat.

Le serveur écoute ensuite en permanence les messages des clients. Si un client quitte le chat, le serveur en informe tous les autres clients et le supprime de la liste des clients connectés.

Les exceptions de flux sont gérées par le bloc try-catch. S'il y a une déconnexion de client sans que le serveur soit prévenu, le thread de serveur correspondant va imprimer un message indiquant la déconncexion inattendue du client, notifier tous les clients, puis arrêter.

La classe ServerMessageReceptor utilise l'objet Message pour envoyer et recevoir des messages entre le client et le serveur. Les messages peuvent être de différents types, tels que QUIT pour quitter le chat, MESSAGE pour envoyer un message à tous les clients connectés, ERROR pour signaler une erreur, etc.

En somme, cette classe gère l'interaction entre un client et le serveur de chat, permettant aux utilisateurs de communiquer entre eux dans le chat de manière sécurisée.

### Classe ServerAdministrator.java

La classe singleton ServerAdministrator dans le package "server" stocke les noms d'utilisateur et les sockets de tous les clients connectés dans une structure de données HashMap, qui permet l'unicité des noms d'utilisateur.

La classe "ServerAdministrator" comprend plusieurs fonctions, notamment :

- "showAllClients", qui affiche la liste de tous les clients connectés au serveur avec leurs noms d'utilisateur et sockets associées.
- "listUsernames", qui renvoie une chaîne de caractères contenant la liste de tous les noms d'utilisateur connectés au serveur.
- "isValid", qui vérifie la validité d'un nom d'utilisateur en recherchant dans la liste des noms d'utilisateur existants.
- "addClient", qui tente d'ajouter un nom d'utilisateur et une socket client dans la liste HashMap. Cette fonction vérifie la validité du nom d'utilisateur et renvoie un booléen indiquant si l'ajout a été réussi ou non.
- "removeClient", qui tente de supprimer un nom d'utilisateur de la liste.
- "tellEveryone", qui envoie un message à tous les sockets de tous les clients connectés au serveur.

Le principal objectif de la classe ServerAdministrator est de gérer la communication entre les clients et le serveur. Les fonctions de la classe "ServerAdministrator" permettent de stocker les informations de chaque client, de vérifier la validité de leurs noms d'utilisateur, d'ajouter ou de supprimer des clients de la liste, et d'envoyer des messages à tous les clients connectés.

## Scénarios

Les codes sources d'application sont partagés sur gitlab.utc sous l'addresse [https://gitlab.utc.fr/sr02-haiyang-david/devoir-1-sr03]. Pour le construire, il est recommender d'utiliser l'IDE Intellij IDEA. Le numéro de port et l'addresse de host dans ChatServer et ChatClient doient être modifiés selon le réseau utilisé. Après que le ChatServer soit lancé, les ChatClient peuvent commencer à connecter le serveur.

![server.png](images/server.png)

![user1.png](images/user1.png)
![user2.png](images/user2.png)

## Conclusion

En conclusion, les cinq classes développées pour le projet de créer un chat sont toutes essentielles pour assurer le bon fonctionnement de l'application. La classe "Client" permet aux utilisateurs de se connecter au serveur en fournissant un nom d'utilisateur et une adresse IP. La classe "Server" s'occupe de gérer les connexions entre les clients et de transmettre les messages reçus à tous les clients connectés. La classe "Message" fournit une structure pour l'échange de messages entre les clients et le serveur, ce qui permet une communication efficace au niveau réseau.

La classe "MessageReceptor" est particulièrement importante car elle gère la réception des messages provenant du serveur ou d'autres clients. Elle est responsable de la gestion des erreurs de communication et de la lecture des données de message reçues.

Enfin, la classe "ServerAdministrator" stocke et gère les informations relatives aux clients connectés, ce qui permet au serveur de savoir à qui envoyer les messages entrants. Elle fournit également des fonctions pour ajouter et supprimer des clients de la liste des utilisateurs connectés.

En somme, ces cinq classes travaillent ensemble pour fournir une fonctionnalité de chat complète et fiable, offrant une expérience de communication agréable et efficace pour les utilisateurs.
